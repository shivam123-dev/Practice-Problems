class Solution {
public:
    int minMovesToSeat(vector<int>& seats, vector<int>& students) {
        sort(seats.begin(), seats.end());
        sort(students.begin(), students.end());
        int count=0, j=0;
        for(int x:seats)
            count = abs(x - students[j++]) + count;
        return abs(count);
    }
};