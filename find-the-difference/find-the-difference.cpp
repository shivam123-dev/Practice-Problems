class Solution {
public:
    char findTheDifference(string s, string t) {
        sort(t.begin(), t.end());
        sort(s.begin(), s.end());
        for(int i=0, j=0;i<s.length(), j<t.length();i++, j++){
            if(s[i]!=t[i]){
                return t[i];
            }
        }
        
        return s[0];
    }
};